package ru.t1.rleonov.tm.api.repository.model;

import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {
}

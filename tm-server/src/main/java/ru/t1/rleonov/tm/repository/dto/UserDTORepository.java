package ru.t1.rleonov.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.rleonov.tm.dto.model.UserDTO;
import javax.persistence.EntityManager;
import java.util.List;

public final class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m ORDER BY login";
        return entityManager.createQuery(jpql, UserDTO.class).getResultList();
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        return findByLogin(login) != null;
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        return findByLogin(email) != null;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

}

package ru.t1.rleonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.dto.request.ServerSaveDataJsonFasterXmlRequest;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";

    @NotNull
    private static final String DESCRIPTION = "Save data in json file using fasterxml.";

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final ServerSaveDataJsonFasterXmlRequest request = new ServerSaveDataJsonFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
